       IDENTIFICATION DIVISION.
       PROGRAM-ID. DoCalc.
      *>  AUTHOR. Khummeung Wattanasaroj
       DATA DIVISION.
       WORKING-STORAGE SECTION.
       01  FirstNum    PIC 9       VALUE ZEROES.
       01  SecondNum   PIC 9       VALUE ZEROES.
       01  CalcResult  PIC 99      VALUE 0.
       01  UserPrompt  PIC X(38)   VALUE
                       "Please enter two single digit number".
       PROCEDURE DIVISION.
       CalculateResult.
           DISPLAY UserPrompt
           ACCEPT FirstNum
           ACCEPT SecondNum
           COMPUTE CalcResult = FirstNum + SecondNum
           DISPLAY "Result is = ", CalcResult
           STOP RUN.
