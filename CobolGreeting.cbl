       IDENTIFICATION DIVISION.
       PROGRAM-ID. CobolGreeting.
       *>  AUTHOR. Khummeung Wattanasaroj
       DATA DIVISION.
       WORKING-STORAGE SECTION.
       01  IterNum PIC 9 VALUE 5.

       PROCEDURE DIVISION.
       BeginProgram.
           PERFORM DisplayGreeting IterNum TIMES.
           STOP RUN.

       DisplayGreeting.
           DISPLAY "Greetings from COBOL".

